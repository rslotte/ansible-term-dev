## ansible-term-dev


`wget -O - https://gitlab.com/rslotte/ansible-term-dev/raw/master/install.sh | bash`

* Set default gnome-terminal to `nord`
* Override shell with `tmux` in gnome-terminal settings
* Run `:PlugInstall` in nvim
* Run `prefix + I` in tmux

