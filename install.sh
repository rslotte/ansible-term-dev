#!/usr/bin/env bash

set -euxo pipefail


PLAY=""

if [[ "${OSTYPE}" == "linux-gnu" ]]; then
  DISTRO=$(cat /etc/os-release | head -n1)
  if [[ "${DISTRO}" == *"Ubuntu"* ]]; then
    sudo apt update
    sudo apt install software-properties-common git -y
    sudo apt-add-repository -y -u ppa:ansible/ansible
    sudo apt install ansible -y
    PLAY="ubuntu.yml"
  elif [[ "${DISTRO}" == *"Fedora"* ]]; then
    sudo dnf update -y
    sudo dnf install ansible -y
    PLAY="fedora.yml"
  fi
elif [[ "${OSTYPE}" == "darwin"* ]]; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  brew install git ansible
  PLAY="darwin.yml"
fi

rm -rf /tmp/ansible-term-dev
git clone https://gitlab.com/rslotte/ansible-term-dev.git /tmp/ansible-term-dev
ansible-playbook -K -u ${USER} /tmp/ansible-term-dev/${PLAY}
rm -rf /tmp/ansible-term-dev
